"""API Inputs validation."""

import re

from pydantic import BaseModel, Field
from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes

RE_SUBARRAY = re.compile(r"^[0-9]{2}$", flags=0)
RE_PROCESSING_BLOCK = re.compile(r"^[A-Za-z0-9\-_]+$", flags=0)
RE_EXECUTION_BLOCK = re.compile(r"^[A-Za-z0-9\-_]+$", flags=0)
RE_DEPLOYMENT = re.compile(r"^[A-Za-z0-9\-_]+$", flags=0)


class CreateWindow(BaseModel):
    """Create a new window."""

    metric: MetricDataTypes = Field(description="The metric to be configured against")
    subarray: str = Field(description="The subarray to link to", pattern=RE_SUBARRAY)
    processing_block: str | None = Field(
        default=None, description="The processing block to link to", pattern=RE_PROCESSING_BLOCK
    )
    spectrum_start: int = Field(
        description="The start of the spectrum",
        gte=1,
    )
    spectrum_end: int = Field(
        description="The end of the spectrum",
        gte=1,
    )
    channels_averaged: int = Field(
        default=1,
        description="The number of channels to average together in the data",
        gte=1,
    )
