"""Run the procedure to do a cleanup of processing block data."""

import argparse
import logging
from time import sleep, time

from redis import Redis, exceptions

from ska_sdp_qa_data_api.server.core.config import (
    FAR_FUTURE,
    FAR_PAST,
    REDIS_HOST,
    REDIS_PORT,
    WATERFALL_CLEANUP_SECONDS,
)
from ska_sdp_qa_data_api.server.utils.helpers import configure_parser_and_logs

logger = logging.getLogger(__name__)


def _seconds_to_time(seconds: float) -> str:
    minutes = seconds // 60
    seconds %= 60
    hours = minutes // 60
    minutes %= 60
    return f"{hours:02.0f}:{minutes:02.0f}:{seconds:06.3f}"


def run_clean_procedure():
    """Run the actual cleanup."""
    waterfall_clean_before = time() - WATERFALL_CLEANUP_SECONDS
    # Connect to Redis
    redis = Redis(host=REDIS_HOST, port=REDIS_PORT)

    # Clean the timeseries data for the waterfall plots
    keys = redis.keys("waterfall:*")
    for key in keys:
        key_decoded = key.decode()
        if redis.type(key).decode() != "zset":
            logger.warning("[%s] is not a timeseries set", key_decoded)
            continue

        record_count = redis.zcount(key, min=FAR_PAST, max=FAR_FUTURE)
        if record_count == 0:
            # This should never happen, but just in case
            continue

        latest_element = redis.zrange(key, -1, -1, withscores=True)[-1][-1]
        start, end, age = (
            FAR_PAST,
            waterfall_clean_before,
            time() - latest_element,
        )

        logger.info(
            "[%s] Records: %d | Latest: %d | Age: %s",
            key_decoded,
            record_count,
            latest_element,
            _seconds_to_time(age),
        )

        records_removed = redis.zremrangebyscore(key, start, end)
        logger.info(
            "[%s] Cleaning [%d, %d] | Cleaned: %d", key_decoded, start, end, records_removed
        )
    if len(keys) == 0:
        logger.info("No waterfall plots found")


def main():  # pragma: no cover
    """Main function."""
    parser = argparse.ArgumentParser(description="Redis Data Cleaner")
    configure_parser_and_logs(parser)

    while True:
        try:
            run_clean_procedure()
        except exceptions.ConnectionError:
            logger.warning("Redis is not avaialble as yet")
        sleep(60)


if __name__ == "__main__":  # pragma: no cover
    main()
